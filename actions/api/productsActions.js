import { Product } from '../../db/models/Product.js'

class ProductsActions{
    
    async getAllProducts(req, res){
        try{
            const doc = await Product.find({})
            res.status(200).json(doc)
        }
        catch (err){
            res.status(500).send('nie udało się popbrać danych')
        }
    }
    async getProduct(req, res){
        const id = req.params.id
        try{
            const doc = await Product.findOne({ _id: id })
            res.status(200).json(doc)
        }
        catch (err){
            res.status(500).send('nie udało się pobrać danych')
        }
    }
    async saveProduct(req, res){
        const name = req.body.product.name
        const category = req.body.product.category
        const quantity = req.body.product.quantity

        let product
        
        try{
            product = new Product({name, category, quantity})
            await product.save()
        }
        catch(err){
            return res.status(422).json({message: err.message})
        }
        res.status(201).json(Product)
    }
    async updateProduct(req, res){
        const id = req.params.id
        const name = req.body.product.name
        const category = req.body.product.category
        const quantity = req.body.product.quantity
        
        try{
            const product = await Product.findOne({ _id: id })
            product.name = name
            product.category = category
            product.quantity = quantity
            await product.save()
            res.status(201).json(Product)
        }
        catch (err){
            res.status(500).send('nie udało się edytować')
        }
    }
    async deleteProduct(req, res){
        const id = req.params.id
        try{
            await Product.deleteOne({  _id: id })
            res.sendStatus(204)
        }
        catch (err){
            res.status(500).send('nie udało się usunąć')
        }
        
    }
}
const productsActions = new ProductsActions
export { productsActions }