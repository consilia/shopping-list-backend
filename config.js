const port =  process.env.PORT || 3001
const database =  process.env.DATABASE || 'mongodb://localhost:27017/shoppingList'
export {port, database}
