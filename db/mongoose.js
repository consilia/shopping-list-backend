import mongoose from 'mongoose'
import { database } from '../config.js'
import { Product } from './models/Product.js' 

mongoose.connect(database, {useNewUrlParser: true, useUnifiedTopology: true});


export {mongoose}