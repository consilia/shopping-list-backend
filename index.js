import express from 'express'
import { port } from './config.js'
import { router } from './routes/api.js'
import { mongoose } from './db/mongoose.js'
import cors from 'cors'

const app = express()

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(cors())
app.use('/', router)

app.listen(port, function(){})