import express from 'express'
import { productsActions } from '../actions/api/productsActions.js'

const router = express.Router();

// pobieranie product
router.get('/products', productsActions.getAllProducts);
// pobieranie producty
router.get('/products/:id', productsActions.getProduct);
// zapisywanie producty
router.post('/products', productsActions.saveProduct);
// edytiwanie producty
router.put('/products/:id', productsActions.updateProduct);
// usuwanie producty
router.delete('/products/:id', productsActions.deleteProduct);

export {router}